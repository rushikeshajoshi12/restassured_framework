package request_repository;

public class Endpoints {

	static String hostname = "https://reqres.in/";

	public static String post_endpoint() {
		String postURL = hostname + "api/users";

		System.out.println(postURL);

		return postURL;
	}

	public static String get_endpoint() {
		String getURL = hostname + "api/users?page=2";

		System.out.println(getURL);

		return getURL;
	}

	public static String patch_endpoint() {
		String patchURL = hostname + "api/users/2";

		System.out.println(patchURL);

		return patchURL;
	}

	public static String put_endpoint() {
		String putURL = hostname + "api/users/2";

		System.out.println(putURL);

		return putURL;
	}

	public static String delete_endpoint() {
		String deleteURL = hostname + "api/users/2";
		System.out.println(deleteURL);
		return deleteURL;
	}
}
