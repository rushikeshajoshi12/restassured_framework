package request_repository;

import java.io.IOException;
import java.util.ArrayList;

import Common_Utility_Package.excel_data_reader;

public class Patch_request_repository extends Endpoints {
 
	public static String Patch_TC1_request() throws IOException {
		ArrayList<String> exceldata = excel_data_reader.Read_Excel_Data("Api_data.xlsx", "Patch_API", "Patch_TC1");
		String req_name=exceldata.get(1);
		String req_job=exceldata.get(2);
		String requestbody="{\r\n"
				+ "    \"name\": \""+req_name+"\",\r\n"
				+ "    \"job\": \""+req_job+"\"\r\n"
				+ "}";
	 return requestbody;
	}
	
}
