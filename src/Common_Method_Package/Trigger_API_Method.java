package Common_Method_Package;
import static io.restassured.RestAssured.given;

import request_repository.Post_request_Repository;

public class Trigger_API_Method extends Post_request_Repository {

	public static int extract_Status_Code(String requestBody, String postURL) {
		 
		int StatusCode = given().header("Content-Type","application/json").body(requestBody)
				 .when().post(postURL)
				 .then().extract().statusCode();
		return StatusCode;
	}

	public static String extract_Responsebody(String requestbody, String postURL) {
	String Responsebody=given().header("Content-Type","application/json").body(requestbody).when().post(postURL)
		 .then().extract().response().asString();
		return Responsebody;
	}
}
