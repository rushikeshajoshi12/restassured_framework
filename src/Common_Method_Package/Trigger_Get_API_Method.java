package Common_Method_Package;

import static io.restassured.RestAssured.given;

import request_repository.Endpoints;

public class Trigger_Get_API_Method extends Endpoints {

	public static int extract_Status_Code(String getURL) {
		 
		int StatusCode = given().when().get(getURL).then().extract().statusCode();
		return StatusCode;
	}
	
	public static String extract_Responsebody(String getURL) {
		String Responsebody=given().when().get(getURL)
				.then().extract().response().asString();
			return Responsebody;
		}
}
