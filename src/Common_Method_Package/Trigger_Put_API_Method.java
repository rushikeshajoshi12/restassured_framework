package Common_Method_Package;

import static io.restassured.RestAssured.given;
import request_repository.Put_request_repository;

public class Trigger_Put_API_Method extends Put_request_repository {

	public static int extract_Status_Code(String requestBody, String putURL) {

		int StatusCode = given().header("Content-Type", "application/json").body(requestBody).when().put(putURL).then()
				.extract().statusCode();
		return StatusCode;
	}

	public static String extract_Responsebody(String requestBody, String putURL) {
		String Responsebody = given().header("Content-Type", "application/json").body(requestBody).when().put(putURL)
				.then().extract().response().asString();
		return Responsebody;
	}
}
