package Common_Method_Package;

import static io.restassured.RestAssured.given;
import request_repository.Patch_request_repository;

public class Trigger_Patch_API_Method extends Patch_request_repository {

	public static int extract_Status_Code(String requestbody, String patchURL) {

		int StatusCode = given().header("Content-Type", "application/json").body(requestbody).when().patch(patchURL).then()
				.extract().statusCode();
		return StatusCode;
	}

	public static String extract_Responsebody(String requestbody, String patchURL) {
		String Responsebody = given().header("Content-Type", "application/json").body(requestbody).when().patch(patchURL)
				.then().extract().response().asString();
		return Responsebody;
	}
}