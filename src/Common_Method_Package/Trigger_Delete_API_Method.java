package Common_Method_Package;

import request_repository.Endpoints;
import static io.restassured.RestAssured.given;

public class Trigger_Delete_API_Method extends Endpoints {
	public static int extract_Status_Code( String deleteURL) {

		int StatusCode = given().when().get(deleteURL).then().extract().statusCode();
		return StatusCode;
	}
	public static String extract_deleteResponsebody(String deleteURL) {
		String deleteResponsebody=given().when().get(deleteURL).then().log().all().extract().asString();
			return deleteResponsebody;
	}

}
