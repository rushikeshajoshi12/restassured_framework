package Testclass_Package;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Method_Package.Trigger_Get_API_Method;
import Common_Utility_Package.Handle_logs;
import io.restassured.path.json.JsonPath;


public class Get_TC1 extends Trigger_Get_API_Method {
	@Test
	static void executor() throws IOException {
		File DirectoryName = Handle_logs.create_log_directory("Get_TC1");
		for (int i = 0; i < 5; i++) {
			int Status_Code = extract_Status_Code(get_endpoint());
			System.out.println(Status_Code);
			if (Status_Code == 200) {
				String ResponseBody = extract_Responsebody(get_endpoint());
				System.out.println(ResponseBody);
				Handle_logs.evidence_creator(DirectoryName, "Get_TC1", get_endpoint(), null, ResponseBody);
				validator(ResponseBody);
				break;
			} else {
				System.out.println("Desired Status Code is invalid hence retry");
			}

		}
	}

	public static void validator(String ResponseBody) {
		String[] Exp_id_Array = { "7", "8", "9", "10", "11", "12" };
		JsonPath jsp_res = new JsonPath(ResponseBody);
		List<Object> res_data = jsp_res.getList("data");
		// String res_page = jsp_res.getString("page");
		int count = res_data.size();
		for (int i = 0; i < count; i++) {
			String Exp_id = Exp_id_Array[i];
			String res_id = jsp_res.getString("data[" + i + "].id");
			AssertJUnit.assertEquals(res_id, Exp_id);

		}
	}

}
