package Testclass_Package;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.File;



import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Method_Package.Trigger_API_Method;
import Common_Utility_Package.Handle_logs;
import io.restassured.path.json.JsonPath;


public class Post_TC1 extends Trigger_API_Method {
	@Test
	/*
	 * public static void main(String[] args) { int Status_Code =
	 * Trigger_API_Method.extract_Status_Code(post_TC1_Request(), post_endpoint());
	 * System.out.println(Status_Code);
	 * 
	 * String
	 * Responsebody=Trigger_API_Method.extract_Responsebody(post_TC1_Request(),
	 * post_endpoint()); System.out.println(Responsebody);
	 */

	public static void executor() throws IOException {
		String requestBody = post_TC1_Request();
		File DirectoryName = Handle_logs.create_log_directory("Post_TC1");
		for (int i = 0; i < 5; i++) {
			int status_Code = extract_Status_Code(requestBody, post_endpoint());
			System.out.println("Status Code: " + status_Code);
			if (status_Code == 201) {
				String ResponseBody = extract_Responsebody(requestBody, post_endpoint());
				System.out.println("Response Body :" + ResponseBody);
				Handle_logs.evidence_creator(DirectoryName, "Post_TC1", requestBody, post_endpoint(), ResponseBody);
				validator(requestBody, ResponseBody);
				break;
			} else {
				System.out.println("Desired Status Code not found hence, retry");
			}
		}
	}
	
	public static void validator(String requestBody, String ResponseBody) {
		JsonPath jsp = new JsonPath(ResponseBody);
		String res_name = jsp.getString("name");
		String res_job = jsp.getString("job");
		//int res_id = jsp.getInt("id");
		String res_createdAt = jsp.getString("CreatedAt");
		// Validation
		AssertJUnit.assertEquals(res_name, "morpheus");
		AssertJUnit.assertEquals(res_job, "leader");
		//AssertJUnit.assertNotNull(res_id, "id");
		AssertJUnit.assertEquals(res_createdAt, res_createdAt);

	}
}