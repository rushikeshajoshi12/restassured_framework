package Testclass_Package;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Method_Package.Trigger_Put_API_Method;
import Common_Utility_Package.Handle_logs;
import io.restassured.path.json.JsonPath;


public class Put_TC1 extends Trigger_Put_API_Method {
	@Test
	/*
	 * public static void main(String[] args) {
	 * 
	 * int Status_Code =
	 * Trigger_Put_API_Method.extract_Status_Code(Put_TC1_request(),
	 * put_endpoint()); System.out.println(Status_Code);
	 * 
	 * String Responsebody =
	 * Trigger_Put_API_Method.extract_Responsebody(Put_TC1_request(),
	 * put_endpoint()); System.out.println(Responsebody); }
	 */

	public static void executor() throws IOException {
		String requestBody = Put_TC1_request();
		File DirectoryName = Handle_logs.create_log_directory("Put_TC1");
		for (int i = 0; i < 5; i++) {
			int status_Code = extract_Status_Code(requestBody, put_endpoint());
			System.out.println("Status Code: " + status_Code);
			if (status_Code == 200) {
				String ResponseBody = extract_Responsebody(requestBody, put_endpoint());
				System.out.println("Response Body :" + ResponseBody);
				Handle_logs.evidence_creator(DirectoryName, "Put_TC1", requestBody, put_endpoint(), ResponseBody);
				validator(requestBody, ResponseBody);

				break;
			} else {
				System.out.println("Desired Status Code not found hence, retry");
			}
		}
	}
	
	public static void validator(String requestBody, String ResponseBody) {
		JsonPath jsp = new JsonPath(ResponseBody);
		String res_name = jsp.getString("name");
		String res_job = jsp.getString("job");
		String res_updatedAt = jsp.getString("res_updatedAt");
		// Validation
		AssertJUnit.assertEquals(res_name, "morpheus");
		AssertJUnit.assertEquals(res_job, "zion resident");
		AssertJUnit.assertEquals(res_updatedAt, res_updatedAt);

	}
}
