package Testclass_Package;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import java.io.File;
import java.io.IOException;

import org.testng.annotations.Test;

import Common_Method_Package.Trigger_Delete_API_Method;
import Common_Utility_Package.Handle_logs;

public class Delete_TC1 extends Trigger_Delete_API_Method {
	@Test
	public static void executor() throws IOException {
		for (int i = 0; i < 5; i++) {
			File DirectoryName = Handle_logs.create_log_directory("Delete_TC1");
			int StatusCode = Trigger_Delete_API_Method.extract_Status_Code (delete_endpoint());
			System.out.println(StatusCode);
			if (StatusCode == 204) {
				String deleteResponsebody = Trigger_Delete_API_Method.extract_deleteResponsebody(null);
				System.out.println(deleteResponsebody);
				Handle_logs.evidence_creator(DirectoryName, "Delete_TC1", null, null, deleteResponsebody);
				validator(deleteResponsebody);
				break;
			} else {
				System.out.println("Desired Status Code not found hence, retry");
			}
		}
	}
	
	 static void validator(String deleteResponsebody) {
		

	}
}
