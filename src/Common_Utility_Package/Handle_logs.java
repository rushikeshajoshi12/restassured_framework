package Common_Utility_Package;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_logs {
	public static File create_log_directory(String DirectoryName) {
		String current_project_directory = System.getProperty("user.dir");
		System.out.println(current_project_directory);
		File Log_directory = new File(current_project_directory + "\\API_Logs\\" + DirectoryName);
		deleteDirectory(Log_directory);
		Log_directory.mkdir();
		return Log_directory;
	}

	public static boolean deleteDirectory(File directory) {
		boolean directorydeleted = directory.delete();
		if (directory.exists()) {
			File[] files = directory.listFiles();
			if (files != null) {
				for (File file : files) {
					if (file.isDirectory()) {
						deleteDirectory(file);
					} else {
						file.delete();
					}
				}
			}
			directorydeleted = directory.delete();
		} else {
			System.out.println("Directory does not exist.");
		}
		return directorydeleted;
	}

	public static void evidence_creator(File directoryname, String filename, String Endpoint, String Requestbody,
			String Responsebody) throws IOException {
		// step1 create the file at given location
		File newfile = new File(directoryname + "\\" + filename + ".txt");
		System.out.println("new file created to save evidence :\" + newfile.getName()");

		// step 2 write data into the file
		FileWriter datawriter = new FileWriter(newfile);
		datawriter.write("Endpoint : " + Endpoint + "\n\n");
		datawriter.write("RequestBody : \n\n" + Requestbody + "\n\n");
		datawriter.write("ResponseBody: \n\n" + Responsebody);
		datawriter.close();
		System.out.println("Evidance is written in file : " + newfile.getName());

	}

}
