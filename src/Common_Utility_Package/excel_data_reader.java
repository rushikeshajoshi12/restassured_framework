package Common_Utility_Package;

import java.io.FileInputStream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class excel_data_reader {

	public static ArrayList<String> Read_Excel_Data(String File_name, String Sheet_name, String Test_case_name)
			throws IOException {
		ArrayList<String> Arraydata = new ArrayList<String>();
		// step1 locate the file
		String project_dir = System.getProperty("user.dir");
		FileInputStream fis = new FileInputStream(project_dir + "\\Input_data\\" + File_name);
		// Step2 access the located file
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		// count the number of sheets available in excel file
		int countofsheet = wb.getNumberOfSheets();
		System.out.println(countofsheet);
		// step4 access the desired sheet
		for (int i = 0; i < countofsheet; i++) {
			String sheetname = wb.getSheetName(i);
			if (sheetname.equals(Sheet_name)) {
				System.out.println("inside the sheet:" + sheetname);
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> Rows = sheet.iterator();
				while (Rows.hasNext()) {
					Row currentrow = Rows.next();
					// step5 access the row corosponding to desire test cases
					if (currentrow.getCell(0).getStringCellValue().equals(Test_case_name)) {
						Iterator<Cell> cell = currentrow.iterator();
						while (cell.hasNext()) {
							String data = cell.next().getStringCellValue();
							System.out.println(data);
							Arraydata.add(data);
						}
					}
				}
			}

		}
		wb.close();
		return Arraydata;
	}
}